## PROYECTO: Diseño, construcción y control de una montura altazimutal - Autores: Daniel Del Mazo Hermida y José María Nieto Royuela

![alt text](http://i65.tinypic.com/288obd3.jpg)

---

## Objetivos del proyecto

1. Diseñar y fabricar la montura de un telescopio mediante impresión 3D.
2. Realizar un manual de montaje detallando paso a paso el proceso de construcción.
3. Realizar un manual de montaje detallando paso a paso el proceso de construcción.
4. Desarrollar un control local que nos permita el movimiento del telescopio mediante teclas direccionales, sin ningún control externo.
5. Desarrollar el software de control de los motores que permita un correcto funcionamiento de los mismos.
6. Desarrollar un software de motorización encargado de múltiples tareas: comunicación serie con Arduino, conversión de coordenadas, puesta en marcha del sistema, etc.
7. Crear un driver de comunicación que nos permita usar la montura desarrollada en múltiples plataformas, facilitando su uso y entendimiento.

---

## Objetivos por proyecto

Los objetivos globales se reparten entre ambos proyectos como se muestra:
“Diseño, construcción y control de una montura altazimutal de fabricación propia para un telescopio” – Autor: José María Nieto Royuela.

1. Diseñar y fabricar la montura de un telescopio mediante impresión 3D, para que cualquier persona pueda imprimir las piezas y montarlo. 
2. Realizar un manual de montaje detallando paso a paso el proceso de construcción.
3. Desarrollar el software de control de los motores que permita un correcto funcionamiento de los mismos.

“Desarrollo del software de control local y remoto para una montura altazimutal de fabricación propia” – Autor: Daniel Del Mazo Hermida.

4. Desarrollar un control local que nos permita el movimiento del telescopio mediante teclas direccionales, sin ningún control externo.
5. Desarrollar un software de motorización encargado de múltiples tareas: comunicación serie con Arduino, conversión de coordenadas, puesta en marcha del sistema, etc.
6. Crear un driver de comunicación que nos permita usar la montura desarrollada en múltiples plataformas, facilitando su uso y entendimiento.

---

## Resumen

El telescopio es un instrumento óptico que nos ha acompañado desde el año 1610 gracias al astrónomo, filósofo, ingeniero, matemático y físico italiano, Galileo Galilei. 
A pesar de sus, aproximadamente, 400 años de historia, existe una barrera hacia la astronomía debido a su gran complejidad en cuanto a conocimiento previo necesario,
lo que impide que un mayor número de personas sienta interés por este espacio de conocimiento. Por este motivo, y para además conseguir que la adquisición de un telescopio 
no suponga un excesivo coste, se inicia este proyecto. 
Buscamos la creación de una montura de bajo coste completamente motorizada que permita a un usuario con un nivel bajo de conocimiento astronómico la posibilidad de contemplar 
todos los astros del firmamento. Para llevar a cabo dicho objetivo, partimos del uso de materiales de fácil acceso y placas de desarrollo hardware tales como Arduino y Raspberry Pi. 
Además, para la creación completa de la montura nos basamos en métodos de fabricación sencillos y baratos: impresoras 3D, las cuales cada día se encuentran más extendidas para uso particular. 

Por una parte, la placa Arduino es utilizada para el control de bajo nivel de la montura: regulación del PID incluido en el sistema para un funcionamiento suave, conteo de los pasos de los
encoders para cada uno de los motores y manejo de las señales de cambio de sentido de los mismos. Por otro lado, la placa Raspberry Pi tendrá un control superior encargado de monitorizar 
todos los procesos realizados en el microcontrolador además de llevar a cabo todas sus tareas pertinentes, entre las que destacan: la puesta en marcha de la montura, el cambio de 
coordenadas entre horizontales a ecuatoriales absolutas, el control de la montura mediante un mando y la comunicación exterior gracias al protocolo INDI. 

Como podemos ver, se trata de un sistema muy completo con comunicación entre microcontrolador y microprocesador, así como una comunicación exterior que permite la conexión de
clientes para facilitar el manejo de la montura completa. En definitiva, la creación de una montura de un telescopio es un proceso muy amplio y que abarca una gran cantidad de campos 
de la ingeniería: diseño y fabricación de prototipos, montaje de los mismos, resolución de problemas en montaje, cableado del sistema completo, creación del código de control de bajo nivel 
y monitorización, comunicación interna y externa de la montura, puesta en marcha de la misma y comprobación de un funcionamiento completo. Así pues, el proyecto abarca de forma global todos 
los ámbitos estudiados y desarrollados durante el máster. 

---
