#!/usr/bin/python3


#Importamos las librerias necesarias

import sys
import RPi.GPIO as GPIO


#Importamos codigos generados por nosotros
from FuncionesGenerales import Lectura_Boton

        

class control_mando ():
    def __init__(self):
        self.west = 0
        self.east = 0
        self.north = 0
        self.south = 0
        self.ok = 0
        self.cancel = 0
        self.modo_funcionamientoAL = 0
        self.modo_funcionamientoAZ = 0
    
    def lectura_botones(self):

        #PROGRAMA DE CONTROL DE MOVIMIENTO DE LOS BOTONES EN MODO ABSOLUTO
        
        self.south = not Lectura_Boton()[0]
        self.north = not Lectura_Boton()[1]
        self.ok = not Lectura_Boton()[2]
        self.cancel = not Lectura_Boton()[3]
        self.east = not Lectura_Boton()[4]
        self.west = not Lectura_Boton()[5]

        self.modo_funcionamientoAL=int(self.north)-int(self.south)
        self.modo_funcionamientoAZ=int(self.east)-int(self.west)
            
            
##        print(self.north, self.south, self.east, self.west)
            
            
            
            
            
        
        
