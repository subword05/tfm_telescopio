# -*- coding: utf-8 -*-
#!/usr/bin/python3

	
##
##Enc1A=17
##Enc1B=22
##
##Enc2A=9
##Enc2B=10
##
##Motor1A = 23
##Motor1B = 24
##Motor1E = 18
## 
##Motor2A = 5
##Motor2B = 6
##Motor2E = 13




class Const:
    #pines encoder
    Enc1A=11  
    Enc1B=13
    #
    Enc2A=21
    Enc2B=19
    #pines motores y pwms
    Motor1A = 16  
    Motor1B = 18
    Motor1E = 12
    #
    Motor2A = 29
    Motor2B = 31
    Motor2E = 33
    #Constantes reducción
    CPR1 = 64 #Clicks per Second Encoder
    GR1 = 50 #Gear ratio caja
    TR_r1 = 1 #Relacion de transmisión de la reductora
    #
    CPR2 = 64 #Clicks per Second Encoder
    GR2 = 50 #Gear ratio caja
    TR_r2 = 7 #Relacion de transmisión de la reductora

    #COORD Polaris
    AR_pol=[2,35,54]#hms
    DEC_pol=[89,16,49]#gms

    #COORD Alfa Centauri
    AR_Acent=[14,39,36.5]
    DEC_Acent=[-60,-50,-13.8] ##CUIDADO LOS NEGATIVOS SON TODOS NEGATIVOS!!


    
