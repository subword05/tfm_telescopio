#!/usr/bin/python3


#Importamos las librerias necesarias

import sys
import time
import socket
import threading
import RPi.GPIO as GPIO
import re
import json
import time
import os

from datetime import datetime
from RPLCD import CharLCD
from urllib.request import urlopen



#Inicializamos la LCD y deshabilitamos warnings por el GPIO

def LCD_ON():
    GPIO.setwarnings(False)
        
    lcd = CharLCD(cols=16, rows=2, pin_rs=37, pin_e=35, pins_data=[36, 31, 29, 23], numbering_mode=GPIO.BOARD)
        
    lcd.clear()
    
    return lcd

#Funciones de lectura de los parámetros de los botones
    
def Button_State():
    #Configuración de los botones
    
    GPIO.setmode(GPIO.BOARD)

    GPIO.setup(11, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(13, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(15, GPIO.IN, pull_up_down=GPIO.PUD_UP)   
    GPIO.setup(16, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP)    
    
#Función encarga de la lectura del valor de los botones
    
def Lectura_Boton():
    
    #BOTÓN DE SOUTH
    input_state_1 = GPIO.input(15)
    
    #BOTÓN DE NORTH
    input_state_2 = GPIO.input(13)
    
    #BOTÓN DE OK
    input_state_3 = GPIO.input(16)
    
    #BOTÓN DE CANCEL
    input_state_4 = GPIO.input(18)
    
    #BOTÓN DE EAST
    input_state_5 = GPIO.input(11)
    
    #BOTÓN DE WEST
    input_state_6 = GPIO.input(22)
    
    return (input_state_1, input_state_2, input_state_3, input_state_4, input_state_5, input_state_6)
 
#Funcion millis que me regula el tiempo de espera no activa en función del valor ingresado

def millis(vel):
    
    t_write = 0
    t_clean = 0
    t_gap = [1000,750, 500, 250, 100, 50]

    t_write = int(round(time.time()*1000));
                
    while True:

        #Hacemos lecturas de tiempo para controlar el tiempo en el que un mensaje esta en pantalla         

        t_clean = int(round(time.time()*1000));
        
        if t_clean - t_write >= t_gap[vel]:
            break

#Función para escribir en la LCD
        
def write_LCD(lcd,x,y,msg,clean):
    
    if clean == 1:
        lcd.clear()
        lcd.cursor_pos = (x,y)
        lcd.write_string(msg)
        
    else:
        lcd.cursor_pos = (x,y)
        lcd.write_string(msg)

def Teclado(lcd, letter):

        lcd.clear()
        rango = len(letter)
        
        for i in range (0,rango):
            
            if i < 16:
                
                lcd.cursor_pos=(0,i)
                lcd.write_string(letter[i])
            
            else:
                lcd.cursor_pos=(1,i-16)
                lcd.write_string(letter[i])

        lcd.cursor_pos=(1,15)
        lcd.write_string('>')
        lcd.cursor_mode = 'blink'
        lcd.cursor_pos=(0,0)
        
        
#Funciones para la realización del scroll lateral
    
def write_to_lcd(lcd, framebuffer, num_cols):
    lcd.home()
    for row in framebuffer:
        lcd.write_string(row.ljust(num_cols)[:num_cols])
        lcd.write_string('\r\n')
                    
                    
def loop_string(string, lcd, framebuffer, row, num_cols, delay=0.15): #DELAY= CONTROLS THE SPEED OF SCROLL
    padding = ' ' * num_cols
    s = padding + string + padding
    for i in range(len(s) - num_cols + 1):
        framebuffer[row] = s[i:i+num_cols]
        write_to_lcd(lcd, framebuffer, num_cols)
        time.sleep(delay)