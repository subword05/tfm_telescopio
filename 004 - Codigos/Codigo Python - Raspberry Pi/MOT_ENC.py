# -*- coding: utf-8 -*-
#!/usr/bin/python3

#Crea un hilo maestro por cada motor encoder, este hilo abre otros dos sub-hilos;
#Uno para el motor y otro para el encoder; mientras el hilo principal recibe
# todos los datos y a través del PID cambia la señal de control del motor

import threading
import time
import sys
import socket
import RPi.GPIO as GPIO


GPIO.setwarnings(False)
GPIO.cleanup()

class Mot:#(threading.Thread):
	def __init__(self,A, B, E):
		#threading.Thread.__init__(self)
		self.A=A
		self.B=B
		self.E=E
		self.SP=0
		self.SP_ant=0
		GPIO.setmode(GPIO.BOARD)
		GPIO.setup(self.A,GPIO.OUT)
		GPIO.setup(self.B,GPIO.OUT)
		GPIO.setup(self.E,GPIO.OUT)
		self.pwm=GPIO.PWM(self.E,7000) ##Frecuencia PWM
		self.pwm.start(0)

		self.flag=1

class Enc:#(threading.Thread):

	def __init__(self, PCA,PCB): #pines de los canales A y B
##		threading.Thread.__init__(self)

		self.PCA=PCA #Pin del canal A del motor ##ESTATICOS
		self.PCB=PCB #Pin del canal B del motor
             
	
		GPIO.setmode(GPIO.BOARD)
		GPIO.setup(self.PCA,GPIO.IN)
		GPIO.setup(self.PCB,GPIO.IN)
		
		self.CA= GPIO.input(self.PCA) #estado del canal A del motor
		self.CB= GPIO.input(self.PCB) #estado del canal B del motor	
		
		self.AB=1
		self.AB_ant=1
		self.Mat=[[0, 1 ,-1, 'E'],[-1 ,0, 'E', 1],[1, 'E', 0 ,-1 ],['E', -1 ,1, 0]] #Matriz encoder		
		
		self.P_time=20*[[0,0]]
		self.P=0 #Pulsos desde el cero :distancia del objetivo al cero en pasos
		
		#self.pwm=50000	
		self.Sentido=1		       		

		#self.lock=threading.Lock()
		self.flag=1	
		
		
class MOTENC(threading.Thread):
	def __init__(self,PCA,PCB,A,B,E,CPR,GR,TR_r):
		threading.Thread.__init__(self)
		self.MOT=Mot(A,B,E)
		self.ENC=Enc(PCA,PCB)
		
		self.pid=43

##		self.ERROR=0
		self.CPR=CPR
		self.GR=GR
		self.TR_r=TR_r
		self.flag=1
		self.TRtot=(360)/(self.CPR*self.GR*self.TR_r) ##GRADOS/CLICK
		
		##grados por paso
##	def run(self):
##		#self.MOT.SP=280
##		I=0
##		Tnow=0
##		Tant=0
##		interv=0.1
##
##		self.MOT.start()
##		self.ENC.start()
##
##		
##		while self.flag:
##			try:
##				Tnow=time.perf_counter()
##				if Tnow>=Tant+interv:
##					Tant=Tnow
##					SP_ant=self.MOT.SP
##
##					P20=self.ENC.P_time[-20][0]
##					t20=self.ENC.P_time[-20][1]
##					P0=self.ENC.P_time[-1][0]
##					t0=self.ENC.P_time[-1][1]
##					#print('PIDDDDDDD')
##					self.pid,self.err,I=self.PID(  P20,t20,P0,t0, I)
##					#self.pid+=1
##					#self.MOT.SP=input('   SP      ')
##					#self.MOT.SP-=0.001
##					if self.MOT.SP=='': self.MOT.SP=SP_ant
##					
##					self.MOT.setSP()
##					
##					#self.MOT.SP=int(self.MOT.SP)
##					if not self.MOT.SP: self.ENC.Sentido=1
##					else : self.ENC.Sentido=self.MOT.SP/abs(self.MOT.SP)
##			except:
##				self.flag=0
##		self.MOT.flag=0
##		self.ENC.flag=0
##		self.MOT.join()
##		self.ENC.join()
##		self.join()
##		print('Saliendo de hilo MOT_ENC')
##
##
##	def PID(self,P20,t20,P0,t0,I):
##		#w=(P_time[-1][0]-P_time[-20][0])/(P_time[-1][1]-P_time[-20][1])#v de giro (w) en pulsos/seg
##		dp=P0-P20
##		dt=t0-t20
##		if dt : v20=dp/dt
##		else :  v20=0
##		
##		err=-self.ERROR
##		#print(err)
##		maxI=10
##		infI=-maxI
##		maxpwm=2000
##		minpwm=0
##		Kp, Ki, Kd= 1 , 0.08 ,0.1
##		P=Kp*err
##		I+=Ki*err*dt
##		D=Kd*v20
##		if abs(err)<200:  I=0
##		elif abs(I)>=maxI:I=maxI
##		PID=round(P+I+D)
##		if PID>=maxpwm: PID=maxpwm
##		elif PID<=minpwm: PID=minpwm
##		if abs(PID)>=100: PID=abs(PID)/(PID)*100
##		elif PID<=0: PID=0
##		if err==0: PID=0

##		return PID , err ,I  
##		
				

	
##
##Enc1A=17
##Enc1B=27
##
##Enc2A=9
##Enc2B=10
##
##Motor1A = 23
##Motor1B = 24
##Motor1E = 18
## 
##Motor2A = 5
##Motor2B = 6
##Motor2E = 13
##CPR=6
##GR=297.92
##TR_r=7
##
##Motenc1=MOTENC(Enc1A,Enc1B,Motor1A,Motor1B,Motor1E,CPR,GR,TR_r)
##Motenc1.start()
##
######try:
##Tnow=0
##Tant=0
##interv=0.2
##try:
##        while True:
##                Tnow=time.perf_counter()
##                if Tnow>=Tant+interv:
##                        Tant=Tnow
##                
##                        print('Dcycle->   '+str(int(Motenc1.MOT.SP))+'  PID->  '+str(Motenc1.pid)+'   P->  '+str(Motenc1.ENC.P)+'  ERROR->   '+str(Motenc1.err))
##                
##except KeyboardInterrupt:
##        GPIO.cleanup()
##        pass
##
##

