ç# -*- coding: utf-8 -*-
#!/usr/bin/python3

from math import  sin, cos ,tan , asin, acos, atan,pi

from datetime import datetime

class COORD:
    def __init__(self):
        self.RA=[0,0,0]
        self.DEC=[0,0,0]
        self.LAT=[0,0,0]
        self.LONG=[0,0,0]
        self.ALT=[0,0,0]
        self.AZ=[0,0,0]
        self.TIME=datetime.now()
    
    def EQabstoALTAZ(self,TIME):
        H, DEC=self.EQabstoEQh(TIME,self.LAT,self.LONG,self.RA,self.DEC)
        ALT , AZ=self.EQhtoALTAZ(self.LAT,H , DEC)
        return ALT, AZ
    
    def ALTAZtoEQabs(self,TIME):
        H,DEC=self.ALTAZtoEQh(self.LAT,self.ALT,self.AZ)
        RA,DEC=self.EQhtoEQabs(TIME,self.LAT,self.LONG,H,DEC)
        return RA,DEC
    
    def GmstoGdec(self,Gr,HtoG=0):
    
        Gdec=(Gr[0]+Gr[1]/60+Gr[2]/3600) # Cambio de sexagesimal a decimal
        if HtoG==1: Gdec*=15 #Si HtoG==1 Cambio de Horas a Grados 360/24=15
        #print(str(Gdec) )
        return Gdec
    
    def GdectoGms(self,Gdec,HtoG=0):
        if HtoG==1: Gdec/=15 #Si HtoG==1 Cambio de Grados a Horas  24/360 = 1/15

        Gr=[0,0,0]
        Gr[0]=int(Gdec)
        Gr[1]=int((Gdec-int(Gdec))*60)
        Gr[2]=((Gdec-int(Gdec))*60-int((Gdec-int(Gdec))*60))*60
        #print(str(Gr))
        return Gr
    
    def ALTAZtoEQh(self,LAT,ALT , AZ): #Recibe altitud [º,',''] y azimut [º,',''] (Horario)
                             #Y devuelve Ascensión Recta (hms) y Declinación [º,',''] (Horario)
        LATr=self.GmstoGdec(LAT)*pi/180
        ALTr=self.GmstoGdec(ALT)*pi/180
        AZr =self.GmstoGdec(AZ )*pi/180

        DECr=asin(-cos(LATr)*cos(AZr)*cos(ALTr)+sin(LATr)*sin(ALTr))
        if (sin(LATr)*cos(AZr)*cos(ALTr)+cos(LATr)*sin(ALTr)) == 0 :   Hr=0
        else:
            Hr=atan((sin(AZr)*cos(ALTr))/(sin(LATr)*cos(AZr)*cos(ALTr)+cos(LATr)*sin(ALTr)))
        DEC=self.GdectoGms(DECr*180/pi)
        H = self.GdectoGms(Hr*180/pi,1)###!!!! Paso a HORAS de Grados 

        return H, DEC

    def EQhtoALTAZ(self,LAT,H , DEC):   #Recibe Ascensión Recta (hms) y Declinación [º,',''](Horario)
                                    #Y devuelve altitud [º,',''] y azimut [º,',''](Horario)
        LATr=self.GmstoGdec(LAT)*pi/180
        Hr  =self.GmstoGdec(H,1)*pi/180 
        DECr=self.GmstoGdec(DEC)*pi/180
        #
        ALTr=asin(cos(LATr)*cos(Hr)*cos(DECr)+sin(LATr)*sin(DECr))
        #AZr=atan((sin(Hr)*cos(DECr))/(cos(LATr)*cos(Hr)*cos(DECr) + cos(LATr)*sin(DECr)))
        if (sin(LATr)*cos(Hr)*cos(DECr) - cos(LATr)*sin(DECr)) == 0: AZr=0
        
        else:   AZr=atan((sin(Hr)*cos(DECr))/(sin(LATr)*cos(Hr)*cos(DECr) - cos(LATr)*sin(DECr)))
    
        ALT=self.GdectoGms(ALTr*180/pi)
        AZ =self.GdectoGms(AZr*180/pi)

        return ALT, AZ

    def SetLST(self,TIME,LAT,LONG):

        Y=TIME.year
        M=TIME.month
        D=TIME.day
        HR=TIME.hour+TIME.minute/60+TIME.second/3600
        
        if M>=3:
            Y-=1
            M+=12
        A=int(Y/100)
        B=2-A+int(A/4)# formula de Jean Meeus en "Astronomical algorithms" 1998
        JD=int(365.25*Y)+int(30.6001*(M+1))+D+HR/24+B+1720994.5#http://personales.unican.es/gonzalmi/ssolar/articulos/calculos.html
        if JD<=2299149.5: JD-=B #dia juliano del 4-Octubre-1582

##          T=(JD- 2415019.5)/36525 # T es el número de siglos julianos de 36525 días medios transcurridos a medianoche de
##                        #Greenwich desde el mediodía medio en Greenwich de 31 de diciembre de 1899.
##          Gr0=((6*3600+38*60+45.836)+8640184.542*T+0.0929*T*T)/3600#Tiempo 0hGr HORAS
##        
##          hJ=24*((Gr0/24)-int(Gr0/24)) #HORA DEL DIA (horas)
##           
##          Gr0hms=self.GdectoGms(Gr0,0) #TSGR A 0h en HMS
##        
##          GrST=hJ+(HR)*1.002737909350795 #TSGr ahora== TSGr0h+ HORA(HDEC)
        T=(JD-2451545)/36525
    
        GrST=(280.46061837+13185000.770053742*T+0.000387933*T*T-T*T*T/38710000)/15 #Horario greenwich en Horas
        
        if GrST<0: GrST+=24
        if GrST>24: GrST-=24
        #print(GrST)
        LONGh=(LONG[0]+LONG[1]/60+LONG[2]/3600)/15  #lONGITUD DE GMS A HDEC
        #print(LONGh)
        LST=GrST+LONGh
        #print(LST)
        
        return LST
    
    def EQhtoEQabs(self,TIME,LAT,LONG,Hh,DEC):
        H=self.GmstoGdec(Hh,0)
        LST=self.SetLST(TIME,LAT,LONG)
        RAh=LST-H
        RA=self.GdectoGms(RAh,0) #DE hDEC a hms
        return RA, DEC
    
    def EQabstoEQh(self,TIME,LAT,LONG,RAh,DEC):
    
        RA=self.GmstoGdec(RAh,0) #HMS a Hdec
        LST=self.SetLST(TIME,LAT,LONG) 
        #LST=13.27946620821
        Hd=LST-RA
        if Hd<0: Hd+=24
        H=self.GdectoGms(Hd,0)
    
        return H , DEC
    
class C_00(COORD):
    def __init__(self,
                 LAT=[40,24,59], #pordefecto coordenadas de madrid
                 LONG=[-3,-42,-9],    #y de polaris
                 TIME=datetime.now(),
                 RA=[2,35,54],
                 DEC=[89,16,49]):
        COORD.__init__(self)
        self.__LAT=LAT
        self.__LONG=LONG
        self.__TIME=TIME
        self.__RA=RA
        self.__DEC=DEC
        
        self.LAT=self.__LAT
        self.LONG=self.__LONG
        self.TIME=self.__TIME
        self.RA=self.__RA
        self.DEC=self.__DEC
        
        self.__ALT,self.__AZ=self.EQabstoALTAZ(self.__TIME)
        

    def RESET_LOC(self,LAT,LONG):
        self.__LAT=LAT
        self.__LONG=LONG
        
        self.LAT=self.__LAT
        self.LONG=self.__LONG
        
   
    def RESET_ORI(self,RA,DEC):
        self.__TIME=datetime.now()
        self.__RA=RA
        self.__DEC=DEC
        
        self.LAT=self.__LAT
        self.LONG=self.__LONG
        self.TIME=self.__TIME
        self.RA=self.__RA
        self.DEC=self.__DEC

        
        
        self.__ALT,self.__AZ=self.EQabstoALTAZ(self.__TIME)
        print('ALT:   '+ str(self.__ALT)+'\t AZ   '+str(self.__AZ))

            
	
class C_obj(COORD):
    def __init__(self,C_00):
        COORD.__init__(self)
        self.ALT0=C_00._C_00__ALT
        self.AZ0=C_00._C_00__AZ

    
    def ACT(self,T):
        self.TIME=T
        self.ALT,self.AZ=self.EQabstoALTAZ(T)
    


class C_Curr(COORD):
    def __init__(self,C_00):
        COORD.__init__(self)
        self.ALT0=C_00._C_00__ALT
        self.AZ0=C_00._C_00__AZ
        self.GALT=0
        self.GAZ=0
    def ACT(self,T):
    
        self.TIME=T
        self.RA,self.DEC=self.ALTAZtoEQabs(T)
    



## RA HORA en hms
## LAT LONG DEC ALT AZ en gms
## gms=hms*15
## gms=rad*180/pi

##C=COORD()
###Pasar de EQabs a ATLAZ
###Datos propios de la observación
##
##Hora=[1,45,20]
##Fecha=[2018,3,19]
##Time=datetime(*(Fecha+Hora))
##LAT_M=[40,24,59]#gms
##LONG_M=[-3,-42,-9]
##
###Datos propios del astro observado
##AR_pol=[2,35,54]#hms
##DEC_pol=[89,16,49]#gms
##
####Pasar de EQabs a EQh
##H_M, DEC_M=C.EQabstoEQh(Time,LAT_M,LONG_M,AR_pol,DEC_pol)
####Pasar de EQh a ALTAZ
##Alt,Az=C.EQhtoALTAZ(LAT_M,H_M , DEC_M)
##
##
##
####Establecer ceros en la alineación    UNA VEZ, PARAMETRO , (EN ESTAS COORDENADAS DADAS (PASADAS A ALT-AZ) X0,Y0, LOS PASOS DADOS SON 0)
####Coger coordenadas (AR-DEC) del objeto  PARAMETRO// A su vez coger las coordenadas del origen (AR-DEC) PARAMETRO//
####Coordenadas AR-DEC -> AZ-ALT HECHO
####A traves de los ceros y los pasos dados saber LA POSICION ACTUAL ->> Pasar los pasos a angulos y sumar a X0,Y0
##
####Coordenadas AZ_ALT, pasadas a seg de arco y hallar Difercia con las coordenadas X0,Y0
####Convertir segundos de grado a pasos mediante TR
#### Calcular el error de posición
##
##
##
##print('ALT: '+ str(Alt)+'  AZ: '+str(Az))
